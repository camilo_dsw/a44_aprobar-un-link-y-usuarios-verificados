
<script src="https://cdn.tailwindcss.com"></script>
@extends('layouts.app')
@section('content')
<h1>
<a href="/community">community</a>
</h1>
@stop

@foreach ($links as $link)
<li>{{$link->title}}</li>
@endforeach
{{$links->links()}}


<h4>Contributed by: {{$link->creator->name}} 
    {{$link->updated_at->diffForHumans()}}</h4>